<?php

return [
    'host' => env('COUCH_HOST'),
    'db'   => env('COUCH_DB'),
];