<?php
if(!function_exists('curl_get')){
    function curl_get($url){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array(
            'Content-Type: application/json',
            'Accept: */*'
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }
}

if(!function_exists('curl_post')){
    /**
     * Get data via curl
     * 
     * @return object
     */
    function curl_post($url, $data, $json_data = true){
        $data = ($json_data) ? json_encode($data) : $data;
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			'Accept: */*'
		));
		$response = curl_exec($ch);
		curl_close($ch);
		return json_decode($response);
    }
}

if(!function_exists('push_fcm')){
    /**
     * Check string is chinese
     * 
     * @return boolean
     */
    function push_fcm($title, $total, $newsID, $type = 'easy'){
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'to' => "/topics/$type-news",
            'notification'   => array(
                "title"  => $title,
            ),
            'data' => array(
                'newsID' => $newsID,
                'total' => $total,
                'type' => $type
            )
        );
        $headers = array(
            'Authorization: key=' . config('app.fcm_key'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        $result = curl_exec($ch);
        curl_close($ch);
    }
}

if(!function_exists('is_korean')){
    function is_korean($text){
        return preg_match('/[\x{3130}-\x{318F}\x{AC00}-\x{D7AF}]/u', $text);
    }
}

//check all characters must be korean
if(!function_exists('must_korean')){
    function must_korean($text){
        return preg_match('/^[\x{3130}-\x{318F}\x{AC00}-\x{D7AF}]+$/u', $text);
    }
}