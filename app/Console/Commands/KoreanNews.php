<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Scraper\CrawlerNews;

class KoreanNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Korean news';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $easy = new CrawlerNews();
        $easy->scraper();
    }
}
