<?php
namespace App\Scraper;
use File;
use PHPOnCouch\CouchClient;
use PHPOnCouch\Exceptions;
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Console\Output\ConsoleOutput;
use Image;

class CrawlerFunction{
    const url_span_topic  = 'http://tool.korean.eupgroup.net/pyapi/seg';
    const get_audio_name  = 'http://tool.korean.eupgroup.net/pyapi/audio_text';
    const url_asset_audio = 'http://tool.korean.eupgroup.net/audios/';
    protected $client;
    protected $couch;
    protected $output;
    protected $message;
    protected $NewsId;
    const news_format = [
        'title' => null,
        'link' => null,
        'description' => null,
        'image' => null,
        'source' => null,
        'date' => null,
        'kind' => null,
        'type' => null,
        'content' => [
            'body' => null,
            'video' => null,
            'audio' => null
        ],
        'level_topik' => null,
        'topik' => null,
        'fieldWords' => null,
        'fields' => null,
    ];

    public function __construct()
    {
        $this->client = new Client(HttpClient::create(['verify_peer' => false, 'verify_host' => false]));
        $this->output = new ConsoleOutput();
        $this->couch = new CouchClient(config('couch.host'),config('couch.db'));
    }

    public function get_content_html($url){
        $crawler = $this->client->request('GET', $url);
        $response = $this->client->getResponse();
        if($response->getStatusCode() == 200){
            return $crawler;
        }
        return false;
    }

    public function exists_news($link){
        $doc = $this->couch->key($link)->getView('search', 'links');
        return ($doc->rows) ? true : false;
    }

    public function general_handle_content($string){
        //remove \n \t \r character
        $string = preg_replace('/(\n|\t|\r|\s{2,})/m','',$string);

        //remove script content
        $string = preg_replace('/<script.*?>.*?<\/script>/im','',$string);

        //remove attribute tags
        $string = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si",'<$1$2>', $string);

        //remove zero-width characters
        $string = str_replace(["&#8203;","\xE2\x80\x8C","\xE2\x80\x8B"],'',$string);

        //remove tags have no content
        $string = preg_replace('/<(\w+)\b(?:\s+[\w\-.:]+(?:\s*=\s*(?:"[^"]*"|"[^"]*"|[\w\-.:]+))?)*\s*\/?>\s*<\/\1\s*>/ixsm',"", $string);
        
        return $string;

    }

    //detach html elements 
    public function detach_element(Crawler $crawler, array $selectors){
        foreach($selectors as $selector){
            $crawler->filter($selector)->each(function (Crawler $craw) {
                foreach ($craw as $node) {
                    $node->parentNode->removeChild($node);
                }
            });
        }
        return $crawler;
    }

    public function news_content_valid($content){
        $content = strip_tags($content);
        $length = count(explode(" ",$content)); 
        $is_corona = $this->is_corona($content);
        return ($length >= 50 && $length <= 2000 && $is_corona == false) ? true : false;
    }

    public function add_span_and_topic($text){
        $body = [
            "text" => $text,
            "type_data" => 'html'
        ];
        $data = curl_post(self::url_span_topic, $body);
        if(isset($data->status) && $data->status == 200){
             //Chỉ lấy 3 fields có nhiều từ nhất
            $array_fields = (array) $data->fields;
            if(count($array_fields) > 3){
                arsort($array_fields);
                $array_fields = array_slice($array_fields,0,3);
                $data->fields = (object) $array_fields;
            }
            return $data;
        }
        return null;
    }

    public function getImage($urlImg, $id)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $urlImg);
        curl_setopt($curl, CURLOPT_TIMEOUT, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4811.0 Safari/537.36 Edg/99.0.1131.3');
        $data = curl_exec($curl);
        curl_close($curl);

        if (!file_exists('public/images/news')) {
            mkdir('public/images/news', 0777, true); //true: cho phép tạo folder lồng nhau
        }
        $name = $id . '.jpg';
        $path = 'images/news/' . $name;
        //resize the image to a width of 800 and constrain aspect ratio (auto height)
        if ($data) {
            $makeImage = Image::make($data)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save("public/$path");
            if (File::exists("public/$path")) {
                return url($path);
            } else return null;
        }
        return null;
    }

    public function getAudio($text){
        $text = strip_tags($text);
        $body = [
            "text" => $text,
            "dir" => '/home/admin/data/audios/',
            "speed" => 80
        ];
        $data = curl_post(self::get_audio_name, $body);
        if(isset($data->status) && $data->status == 200){
            return self::url_asset_audio. reset($data->audio);
        }
        return null;
    }

    public function is_corona($string)
    {
        $pattern = '/(corona|covid|astrazeneca|astra zeneca|sputnik v|sinopharm|verocell|vero cell|pfizer|biontech|moderna|johnson & johnson|aka janssen|Teaching Topics)/im';
        return preg_match($pattern, $string);
    }

    protected function get_level($text){
        $text = preg_replace('/(\n+\s+)/', '', $text);
        $result = [
            'level_topik' => [
                '1' => [],
                '2' => [] ,
                'unknown' => []
            ],
            'topik' => [
                '1' => 0,
                '2' => 0,
                'unknown' => 0
            ]
        ];
        $params = [
            'text' => $text,
            'type_data' => 'json'
        ];
        $response = curl_post(self::url_span_topic, $params);
        if($response->status == 200){
            foreach($response->seg as $item){
                $topik = is_null($item->lv_topik) ? 'unknown' : $item->lv_topik;
                if(must_korean($item->word)){
                    $result['level_topik'][$topik][] = $item->word;
                }
            }
            try{
                foreach($result['level_topik'] as $key => $value){
                    $result['level_topik'][$key] = array_values(array_unique($value));
                }

                foreach($result['level_topik'] as $key => $value){
                    $result['topik'][$key] = count($value);
                }
            }catch(\Exception $e){
                $this->output->writeln("Cannot unique array");
            }
        }
        return $result;
    }

    public function store_news($news){
        try {
            if(!$this->exists_news($news['link'])){
                $news = json_decode(json_encode($news));

                $new = $this->couch->storeDoc($news);
                if(isset($new->ok) && $new->ok == true){
                    $audio = $this->getAudio($news->content->body);
                    $image = $this->getImage($news->image, $new->id);

                    $doc = $this->couch->getDoc($new->id);

                    $doc->image = $image;
                    $doc->content->audio = $audio;
                    $update = $this->couch->storeDoc($doc);

                    $this->message = strip_tags($news->title);
                    $this->newsID = $new->id;

                    return true;
                }
            }
            return false;
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: store_news(CrawlerFunction.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
            return false;
        }
    }
}