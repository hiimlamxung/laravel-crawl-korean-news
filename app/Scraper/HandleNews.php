<?php
namespace App\Scraper;

use Symfony\Component\DomCrawler\Crawler;

class HandleNews extends CrawlerFunction{
    public function get_news_kids_donga($crawler, $url){
        $total = 0;
        try {
            $crawler->filter('.article_list ul.article li')->each(function (Crawler $node) use (&$total) {
                $link   = $node->filter('a')->attr('href');
                if(!$this->exists_news($link)){
                    $description  = $this->add_span_and_topic($node->filter('.at_cont')->text())->seg;
                    $date = trim($node->filter('.article dd:last-child')->text());
                    $crawler_detail_news = $this->get_content_html($link);
                    $detail_news = $this->get_dt_news_kids_donga($crawler_detail_news, $description);

                    $body = $detail_news['content'];
                    if(!$this->news_content_valid($body)){
                        return false;
                    }

                    $news = parent::news_format;
                    $news['title']  = $detail_news['title'];
                    $news['link']   = $link;
                    $news['description']   = $description;
                    $news['image']   = $detail_news['link_img'];
                    $news['source']   = 'Kids donga';
                    $news['date'] = $date;
                    $news['kind'] = null;
                    $news['type'] = 'easy';
                    $news['content']['body'] = $detail_news['content'];
                    $news['level_topik'] = $detail_news['level_topik'];
                    $news['topik'] = $detail_news['topik'];
                    $news['fieldWords'] = $detail_news['fieldWords'];
                    $news['fields'] = $detail_news['fields'];

                    $store = $this->store_news($news); 
                    if($store){
                        $total += 1;
                    }
                }
                
            });
            $this->output->writeln("Imported $total news from $url");
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_news_kids_donga(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_dt_news_kids_donga($crawler, $description){
        try {
            $title = $this->add_span_and_topic($crawler->filter('.at_info>ul>li.title')->text())->seg;
            $link_img = $crawler->filter('.at_content img:first-child')->attr('src');

            //clean content
            $del_elements = [
                "iframe",
                "span[style*='font-size: 10pt;']",
                "p[style*='line-height: 1.8; float: left; width: 52%; margin: 20px; clear: both;']"
            ];
            $crawler = $this->detach_element($crawler, $del_elements);
            $contents = $crawler->filter('.at_content')->html();
            $contents = $this->general_handle_content($contents);
            $contents = preg_replace('/(<(?!\/)[^>]+>)+▶.*/', '', $contents);
            $contents = strip_tags($contents,"<p><span><br><strong><ol><ul><li><b>");

            $span_topic = $this->add_span_and_topic($contents);
            $level = $this->get_level($title . ". ". $description . " " .$contents);

            return [
                'title' => $title,
                'link_img' => $link_img,
                'content' => $span_topic->seg,
                'fieldWords' => $span_topic->fieldWords,
                'fields' => $span_topic->fields,
                'level_topik' => $level['level_topik'],
                'topik' => $level['topik']
            ];
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_dt_news_kids_donga(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_news_kid_chosun($crawler, $url){
        $total = 0;
        try {
            $base_url = 'http://kid.chosun.com';
            $crawler->filter('ul.list-news li.item')->each(function (Crawler $node) use ($base_url, &$total) {
                $link   = $node->filter('.thumb-img>a')->attr('href');
                $link   = (strpos($link,'http') === false) ? $base_url.$link : $link;
                // $description  = trim($node->filter('.con>.desc')->text());
                // $description  = ($description == '') ? null : $description;
                // $title = $node->filter('.con>.subject>a')->text();
                if(!$this->exists_news($link)){
                    $date = trim($node->filter('.con>.date_author>span.date')->text());
                    $date = str_replace('.','-',$date) . ':00'; 
                   
                    $crawler_detail_news = $this->get_content_html($link);
                    $detail_news = $this->get_dt_news_kid_chosun($crawler_detail_news);
                    
                    $body = $detail_news['content'];
                    //k lấy báo ko có ảnh đại diện hợp lệ hoặc content k phù hợp
                    if(!$this->news_content_valid($body) || $detail_news['link_img'] == null){
                        return false;
                    }
    
                    $news = parent::news_format;
                    $news['title']  = $detail_news['title'];
                    $news['link']   = $link;
                    $news['image']   = $detail_news['link_img'];
                    $news['source']   = 'Kid chosun';
                    $news['date'] = $date;
                    $news['kind'] = null;
                    $news['type'] = 'easy';
                    $news['content']['body'] = $detail_news['content'];
                    $news['level_topik'] = $detail_news['level_topik'];
                    $news['topik'] = $detail_news['topik'];
                    $news['fieldWords'] = $detail_news['fieldWords'];
                    $news['fields'] = $detail_news['fields'];
    
                    $store = $this->store_news($news); 
                    if($store){
                        $total += 1;
                    }
                }   
            });
            $this->output->writeln("Imported $total news from $url");
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_news_kid_chosun(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_dt_news_kid_chosun($crawler){
        try {
            $link_img = $crawler->filter("#artImg0")->count();
            $title = $this->add_span_and_topic($crawler->filter(".page_title_subject")->text())->seg;
            if($link_img){
                $link_img = $crawler->filter("#artImg0")->attr('src');
            }else{
                return null;
            }

            $del_elements = [
                "iframe",
                "dt"
            ];
            $crawler = $this->detach_element($crawler, $del_elements);
            
            $contents = '';
            $crawler->filter('.Paragraph')->each(function (Crawler $paragraph) use (&$contents){
                $contents .= $paragraph->html();
            });

            //handle contents
            $contents = $this->general_handle_content($contents);
            $contents = preg_replace('/(<(?!\/)[^>]+>)*<strong>●.*/', '', $contents);
            $contents = strip_tags($contents,"<p><span><br><strong><ol><ul><li><b>");

            $span_topic = $this->add_span_and_topic($contents);
            $level = $this->get_level($title. ". ". $contents);

            return [
                'title' => $title,
                'link_img' => $link_img,
                'content' => $span_topic->seg,
                'fieldWords' => $span_topic->fieldWords,
                'fields' => $span_topic->fields,
                'level_topik' => $level['level_topik'],
                'topik' => $level['topik']
            ];
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_dt_news_kid_chosun(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_news_voakorea($crawler, $url){
        $total = 0;
        try {
            $base_url = 'https://www.voakorea.com';
            $crawler->filter('ul#ordinaryItems li')->each(function (Crawler $node) use ($base_url, &$total) {
                $link   = $node->filter('.media-block__content>a')->attr('href');
                $link   = (strpos($link,'http') === false) ? $base_url.$link : $link;
                if(!$this->exists_news($link)){
                    $description  = $node->filter('.media-block__content>a>p')->text();
                    $title = $node->filter('.media-block__content>a>h4.media-block__title')->text();
    
                    //format date
                    $date = trim($node->filter('span.date')->text());
                    $date = trim(preg_replace('/[^0-9]+/','-',$date),'-'); 
                    $date = date_format(date_create($date), "Y-m-d H:i:s");
                    
                    $crawler_detail_news = $this->get_content_html($link);
                    $detail_news = $this->get_dt_news_voakorea($crawler_detail_news, $title, $description);
                    
                    $body = $detail_news['content'];
                    if(!$this->news_content_valid($body)){
                        return false;
                    }
    
                    $news = parent::news_format;
                    $news['title']  = $this->add_span_and_topic($title)->seg;
                    $news['link']   = $link;
                    $news['description']   = $this->add_span_and_topic($description)->seg;
                    $news['image']   = $detail_news['link_img'];
                    $news['source']   = 'VOA Korea';
                    $news['date'] = $date;
                    $news['kind'] = null;
                    $news['type'] = 'easy';
                    $news['content']['body'] = $detail_news['content'];
                    $news['level_topik'] = $detail_news['level_topik'];
                    $news['topik'] = $detail_news['topik'];
                    $news['fieldWords'] = $detail_news['fieldWords'];
                    $news['fields'] = $detail_news['fields'];
    
                    $store = $this->store_news($news); 
                    if($store){
                        $total += 1;
                    }
                }
            });
            $this->output->writeln("Imported $total news from $url");
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_news_kid_chosun(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_dt_news_voakorea($crawler, $title, $description){
        try {
            $link_img = $crawler->filter('figure.media-image>div.img-wrap>.thumb>img')->attr('src');
            $link_img = preg_replace('/_w[0-9]+_/mi', '_w1000_', $link_img);
            //clean content
            $del_elements = [
                "iframe"
            ];
            $crawler = $this->detach_element($crawler, $del_elements);
            $contents = $crawler->filter('#article-content>.wsw')->html();

            //handle contents
            $contents = $this->general_handle_content($contents);
            $contents = preg_replace('/(<(?!\/)[^>]+>)+\*\s*이 기사는.*/', '', $contents); //remove text *이 기사는 ...
            $contents = preg_replace('/(<(?!\/)[^>]+>)+VOA 뉴스.*/', '', $contents); //remove text VOA 뉴스 ...
            $contents = strip_tags($contents,"<p><span><br><strong><ol><ul><li><b><h2>");

            $span_topic = $this->add_span_and_topic($contents);
            $level = $this->get_level($title . ". ". $description. $contents);

            return [
                'link_img' => $link_img,
                'content' => $span_topic->seg,
                'fieldWords' => $span_topic->fieldWords,
                'fields' => $span_topic->fields,
                'level_topik' => $level['level_topik'],
                'topik' => $level['topik']
            ];
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_dt_news_voakorea(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_news_joongang($crawler,$url){
        $total = 0;
        try {
            $crawler->filter('div.card.card_right_rect')->each(function (Crawler $node) use (&$total){
                $title = $node->filter('.card_body>h2.headline>a')->text();
                $link   = $node->filter('.card_image>figure>a')->attr('href');
                if(!$this->exists_news($link)){
                    $find_description  = $node->filter('.card_body>p.description>a')->count();
                    $description = ($find_description) ? $node->filter('.card_body>p.description>a')->text() : null;

                    $crawler_detail_news = $this->get_content_html($link);
                    $detail_news = $this->get_dt_news_joongang($crawler_detail_news, $title, $description);

                    $body = $detail_news['content'];
                    if(!$this->news_content_valid($body)){
                        return false;
                    }

                    $news = parent::news_format;
                    $news['title']  = $this->add_span_and_topic($title)->seg;
                    $news['link']   = $link;
                    $news['description']   = ($description) ? $this->add_span_and_topic($description)->seg : null;
                    $news['image']   = $detail_news['link_img'];
                    $news['source']   = 'Joongang';
                    $news['date'] = $detail_news['date'];
                    $news['kind'] = null;
                    $news['type'] = 'normal';
                    $news['content']['body'] = $detail_news['content'];
                    $news['level_topik'] = $detail_news['level_topik'];
                    $news['topik'] = $detail_news['topik'];
                    $news['fieldWords'] = $detail_news['fieldWords'];
                    $news['fields'] = $detail_news['fields'];

                    $store = $this->store_news($news); 
                    if($store){
                        $total += 1;
                    }
                }
            });
            $this->output->writeln("Imported $total news from $url");
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_news_chosun(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_dt_news_joongang($crawler, $title, $description){
        try {
            $link_img = $crawler->filter('#article_body>div.ab_photo.photo_center>.image>img')->attr('data-src');
            
            $date = trim($crawler->filter('.time_bx>p.date')->text());
            $date = str_replace('입력 ','',$date);
            $date = str_replace('.','-',$date).':00'; 
            //clean content
            $del_elements = [
                "iframe",
                ".ab_photo.photo_center",
                ".ab_related_article",
                ".ab_byline",
                ".ab_box_article",
                ".ab_player.ovp_player"
            ];
            $crawler = $this->detach_element($crawler, $del_elements);
            $contents = $crawler->filter('#article_body')->html();

            //handle contents
            $contents = $this->general_handle_content($contents);
            $contents = strip_tags($contents,"<p><span><br><strong><ol><ul><li><b>");

            $span_topic = $this->add_span_and_topic($contents);

            $description = is_null($description) ? '' : $description;
            $level = $this->get_level($title . '. '. $description . '. ' .$contents);

            return [
                'link_img' => $link_img,
                'date' => $date,
                'content' => $span_topic->seg,
                'fieldWords' => $span_topic->fieldWords,
                'fields' => $span_topic->fields,
                'level_topik' => $level['level_topik'],
                'topik' => $level['topik']
            ];
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_dt_news_joongang(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_news_donga($crawler,$url){
        $total = 0;
        try {
            $crawler->filter('.mainnews_cont>.mainnews_group>.group')->each(function (Crawler $node) use (&$total){
                $title = $node->filter('.cont_info>.head_title>.tit>a')->text();
                $title = str_replace('단독','',$title); 
                
                $link   = $node->filter('.cont_info>.head_title>.tit>a')->attr('href');
                if(!$this->exists_news($link)){
                    $find_description = $node->filter('.cont_info>.desc>a')->count();
                    $description = ($find_description) ? $node->filter('.cont_info>.desc>a')->text() : null;

                    $crawler_detail_news = $this->get_content_html($link);
                    $detail_news = $this->get_dt_news_donga($crawler_detail_news, $title, $description);
                    
                    $body = $detail_news['content'];
                    if(!$this->news_content_valid($body) || is_null($detail_news['link_img'])){
                        return false;
                    }

                    $news = parent::news_format;
                    $news['title']  = $this->add_span_and_topic($title)->seg;
                    $news['link']   = $link;
                    $news['description']   = ($description) ? $this->add_span_and_topic($description)->seg : null;
                    $news['image']   = $detail_news['link_img'];
                    $news['source']   = 'Donga';
                    $news['date'] = $detail_news['date'];
                    $news['kind'] = null;
                    $news['type'] = 'normal';
                    $news['content']['body'] = $detail_news['content'];
                    $news['level_topik'] = $detail_news['level_topik'];
                    $news['topik'] = $detail_news['topik'];
                    $news['fieldWords'] = $detail_news['fieldWords'];
                    $news['fields'] = $detail_news['fields'];

                    $store = $this->store_news($news); 
                    if($store){
                        $total += 1;
                    }
                }
            });
            $this->output->writeln("Imported $total news from $url");
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_news_donga(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }

    public function get_dt_news_donga($crawler, $title, $description){
        try {
            $find_link_img = $crawler->filter('span.thumb>img')->count();
            $link_img = ($find_link_img) ? $crawler->filter('span.thumb>img')->attr('src') : null;
            
            $date = trim($crawler->filter('.title_foot>.date01:first-child')->text());
            $date = str_replace('입력 ','',$date).':00';
            
            //clean content
            $del_elements = [
                "iframe",
                ".articlePhotoC",
                ".articlePhotoR",
                ".articlePhotoL",
                ".txtRight",
                ".article_footer"
            ];
            $crawler = $this->detach_element($crawler, $del_elements);
            $contents = $crawler->filter('.article_txt')->html();

            //handle contents
            $contents = $this->general_handle_content($contents);
            $contents = strip_tags($contents,"<p><span><br><strong><ol><ul><li><b>");
            $contents = str_replace("<br><br>","<br>", $contents);
            $contents = preg_replace('/<br>([^<>])*?@donga\.com.*/m','', $contents); //remove author's email at the end of the news
           
            $span_topic = $this->add_span_and_topic($contents);
            $description = is_null($description) ? '' : $description;
            $level = $this->get_level($title . '. '. $description . '. ' .$contents);

            return [
                'link_img' => $link_img,
                'date' => $date,
                'content' => $span_topic->seg,
                'fieldWords' => $span_topic->fieldWords,
                'fields' => $span_topic->fields,
                'level_topik' => $level['level_topik'],
                'topik' => $level['topik']
            ];
        } catch (\Exception $e) {
            $this->output->writeln('Break at function: get_dt_news_donga(HandleNews.php) ' . $e->getMessage() . ' --- Errors at: ' . $e->getFile() . ' ---- Line:' . $e->getLine());
        }
    }
}