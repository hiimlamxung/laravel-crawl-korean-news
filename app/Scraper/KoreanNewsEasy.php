<?php
namespace App\Scraper;

use PHPOnCouch\CouchClient;
use PHPOnCouch\Exceptions;
use Goutte\Client as GoutteClient;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Console\Output\ConsoleOutput;

class KoreanNewsEasy extends CrawlerFunction{
    const urlEasy   = 'http://kids.donga.com/';
    const urlAudio  = 'https://readspeaker.jp/ASLCLCLVVS/JMEJSYGDCHMSMHSRKPJL/';
    const num       = 1;
    protected $message;
    protected $NewsId;

    public function __construct()
    {
        parent::__construct();
    }

    public function scraper(){
        $get_easyNews = $this->crawlerEasyNews(self::num);
    }

    public function crawlerEasyNews($num){
        $total = 0;
        try{
            $this->output->writeln('Running easy');
            $url = self::urlEasy . '?page_no' . $num . '&ptype=article&psub=online&gbn=';
            $crawler = $this->get_content_html($url);
            if($crawler !== false){
                $crawler->filter('ul.article li')->each(function (Crawler $node) use (&$total){
                    $link = $node->filter('dt.at_title a')->attr('href');
                    $desc = $node->filter('dd.at_cont')->text();
                    $date = $node->filter('dd[style]')->text();
                    if(!$this->exists_news($link)){
                        $detail = $this->get_detail_new($link,$desc,$date);
                        if($detail == null){
                            return false;
                        }
                        $store = $this->store_new($detail);;
                        if(isset($store->ok)){
                            $this->message = $detail['title'];
                            $this->NewsId  = $store->id;
                            $total += 1;
                        }
                    }
                });
                $this->output->writeln("Imported $total easy news from " . self::urlEasy);

                //put notifycation
                push_fcm($this->message, $total, $this->NewsId);
                $this->output->writeln("Push notification $total easy news");
            }else{
                $this->output->writeln("Cannot get news");
            }
        }catch(\Exception $e){
            echo $e->getMessage() . ' --- Errors at: ' . $e->getTrace()[0]['file'] ."\n Line:" . $e->getTrace()[0]['line']."\n";
        }
    }


    public function get_detail_new($link,$desc,$date){
        $new =  $this->get_content_html($link);
        if($new !== false){
            $at_content = '';
            $at_content_node = $new->filter('.at_content p')->each(function(Crawler $node) use (&$at_content){
                $at_content .= $node->text();
            });
            $title          = $new->filter('div.at_info>ul>li.title')->text();
            $sub_title      = $new->filter('p.at_sub_title')->text();
            
            $images = [];
            $new->filter('div.at_content img')->each(function(Crawler $node) use (&$images){
                $image = $node->attr('src');
                if(strpos( $image,'/www/data/article/')){
                    $images[] = $image;
                }
            });
            if($at_content == ''){
                $at_content = $new->filter('div.at_content p:first-child')->text();
            }
            $at_content = $this->clean($at_content);
            if($at_content != '' && !preg_match('/▶어린이동아/', $at_content) ){
                $word = $this->tokenizer($at_content);
                $data = [
                    'title'         => $title, 
                    'link'          => $link, 
                    'pubDate'       => $date, 
                    'description'   => $desc, 
                    'content'       => [
                        'images'        => $images, 
                        'sub_title'     => $sub_title,
                        'at_content'    => trim($at_content),
                    ],
                    'audio'     => null,
                    'type'      => 'easy',
                    'word'      => $word
                ];
    
                return $data;
            }

        }else return null;
    }


    function clean($str){
        // $str = str_replace('/<p>▶(.|\n)*?@donga.com(.|\n)*?<\/p>/gm', '',$str);
        $str = strip_tags($str);
        $str = preg_replace('/\\t|\\r|\\n/','',$str);
        $text_info = strstr($str, '▶'); // Cắt chuỗi link quảng cáo - category
        $str = str_replace($text_info, '',$str);
        $str = strip_tags($str);
        $str = str_replace('/<p style="color:\s*rgb\(255, 108, 0\);.*?<\/p>/gm', '',$str);
        $str = str_replace('/[\w\s\d\.,;!@#$%^&*()?:\'’]+=AP뉴시스/gm', '',$str);
        $str = str_replace('/<div class=\"ab_photo photo_center \".*?>.*?<\/p>\s+<\/div>/gm', '',$str);
        $str = str_replace('/(<br>|&nbsp;)\s*(.){1,15}기자.*?@(joongang|joongagn).*?<br>/gm', '',$str);
        $str = str_replace('/<div.*?id=\"criteo_network\">.*?<\/div>/gm', '',$str);
        $str = str_replace('/<img id=\"ja_read_tracker\".+?>/gm', '',$str);
        $str = str_replace('/<!--.*?-->/gm', '',$str);
        $str = str_replace('/<div class=\"ab_related_article\">.*?<\/ul>(\s|\n)*<\/div>(\s|\n)*<\/div>/gm', '',$str);
        return $str;
    }
}