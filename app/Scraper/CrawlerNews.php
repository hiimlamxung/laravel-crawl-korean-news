<?php
namespace App\Scraper;

use Symfony\Component\DomCrawler\Crawler;
use App\Scraper\HandleNews;

class CrawlerNews extends CrawlerFunction{
    const list_url_easy   = [
        'http://kids.donga.com/?ptype=article&psub=online',
        'http://kid.chosun.com/list_kj.html?catid=1',
        'https://www.voakorea.com/z/2698'
    ];
    const list_url_normal   = [
        'https://www.joongang.co.kr/',
        'https://www.donga.com/'
    ];
    protected $handle;

    public function __construct()
    {
        parent::__construct();
        $this->handle = new HandleNews();
    }

    public function scraper(){
        // easy
        $this->output->writeln(str_repeat("#", 20). " START GET NEWS EASY " . str_repeat("#", 20));
        foreach(self::list_url_easy as $url ){
            $this->output->writeln(str_repeat("=", 10) . "   Start get data from $url   " . str_repeat("=", 10));
            $crawler = $this->get_content_html($url);
            if ($crawler !== false) {
                switch ($url) {
                    case 'http://kids.donga.com/?ptype=article&psub=online':
                        $list = $this->handle->get_news_kids_donga($crawler, $url);
                        break;

                    case 'http://kid.chosun.com/list_kj.html?catid=1':
                        $list = $this->handle->get_news_kid_chosun($crawler, $url);
                        break;

                    case 'https://www.voakorea.com/z/2698':
                        $list = $this->handle->get_news_voakorea($crawler, $url);
                        break;
                }

            } else {
                $this->output->writeln("Fail !!!");
            }
        }

        //normal
        $this->output->writeln(str_repeat("#", 20). " START GET NEWS NORMAL " . str_repeat("#", 20));
        foreach(self::list_url_normal as $url ){
            $this->output->writeln(str_repeat("=", 10) . "   Start get data from $url   " . str_repeat("=", 10));
            $crawler = $this->get_content_html($url);
            if ($crawler !== false) {
                switch ($url) {
                    case 'https://www.joongang.co.kr/':
                        $list = $this->handle->get_news_joongang($crawler, $url);
                        break;

                    case 'https://www.donga.com/':
                        $list = $this->handle->get_news_donga($crawler, $url);
                }

            } else {
                $this->output->writeln("Fail !!!");
            }
        }
    }

}