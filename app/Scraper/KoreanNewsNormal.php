<?php
namespace App\Scraper;

use PHPOnCouch\CouchClient;
use PHPOnCouch\Exceptions;
use Goutte\Client as GoutteClient;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Console\Output\ConsoleOutput;


class KoreanNewsNormal extends CrawlerFunction{
    const urlNormal = 'https://joongang.joins.com/';

    public function __construct()
    {
        parent::__construct();
    }

    public function scraper(){
        $get_easyNews = $this->crawlerNormalNews();
    }

    public function crawlerNormalNews(){
        $total = 0;
        $omitted = 0;
        try{
            $this->output->writeln('Running normal');
            $url = self::urlNormal;
            $crawler = $this->get_content_html($url);
            if($crawler !== false){
                $crawler->filter('div#body div.section_mainarticle.v2 .type1>ul.list_vertical>li')->each(function (Crawler $node) use (&$total, &$omitted){
                    $link = $node->filter('a[href]:first-child')->attr('href');
                    if(!strpos($link, 'joins.com')){
                        $omitted += 1;
                        $this->output->writeln("a invalid news mitted, link: $link");
                        return false;
                    }
                    $desc = ($node->filter('a[href]:nth-child(2)>p>span')->count() > 0) 
                    ? trim($node->filter('a[href]:nth-child(2)>p>span')->text()) : (($node->filter('a[href]:nth-child(3)>p>span')->count() > 0) 
                    ? trim($node->filter('a[href]:nth-child(3)>p>span')->text()) : trim($node->filter('ul.list_disc')->text()));
                    if(!$this->exists_news($link)){
                        $detail = $this->get_detail_new($link,$desc);
                        $store = $this->store_new($detail);
                        if($store){
                            $total += 1;
                        }
                    }
                });
                $this->output->writeln("Imported $total normal news from " . self::urlNormal);
                ($omitted) ? $this->output->writeln("$omitted news missed") : '';
            }else{
                $this->output->writeln("Cannot get news");
            }
        }catch(\Exception $e){
            echo $e->getMessage() . ' --- Errors at: ' . $e->getTrace()[0]['file'] ."\n  Line:" . $e->getTrace()[0]['line']."\n";
        }
    }

    public function get_detail_new($link,$desc){
        $new =  $this->get_content_html($link);
        if($new !== false){
            $at_content = $new->filter('div#content div#article_body')->html();
            $at_content = $this->handle_at_content($at_content);

            $date       = ($new->filter('div#body div.article_head div.byline em:nth-child(2)')->count()) 
            ? $new->filter('div#body div.article_head div.byline em:nth-child(2)')->text() : $new->filter('.byline div em.mg:first-child')->text();
            $date       = str_replace('입력 ','',$date);
            $date       = str_replace('.','-',$date);

            $title      = ($new->filter('div#body div.article_head h1')->count()) 
            ? $new->filter('div#body div.article_head h1')->text() : $new->filter('#article_title')->text();
            
            $images = [];
            $new->filter('div#content div#article_body .ab_photo.photo_center img')->each(function(Crawler $node) use (&$images){
                $image = ($node->attr('src')) ?? $node->attr('data-src');
                    if($image){
                        $images[] = $image;
                    }
            });
            
            $word = $this->tokenizer($at_content);
            $data = [
                'title'         => $title, 
                'link'          => $link, 
                'pubDate'       => $date, 
                'description'   => $desc, 
                'content'       => [
                    'images'        => $images, 
                    'sub_title'     => null,
                    'at_content'    => trim($at_content),
                ],
                'audio'     => null,
                'type'      => 'normal',
                'word'      => $word
            ];

            return $data;

        }else return null;
    }

    public function handle_at_content($at_content){
        $doc = new \DOMDocument();
        $doc->loadHTML('<?xml version="1.0" encoding="utf-8">' .$at_content);
        $aElements_div = $doc->getElementsByTagName('div');
        $aElements_ul = $doc->getElementsByTagName('ul');
        foreach($aElements_div as $e){
            if($e->getAttribute('class') == 'ab_photo photo_center '){
                $e->parentNode->removeChild($e);
            }
        }
        foreach($aElements_ul as $u){
            if($u->getAttribute('class') == 'text_type'){
                $u->parentNode->removeChild($u);
            }
        }
        $result = $doc->textContent;

        $result = trim(strip_tags($result));
        $result = preg_replace('/\\n|\\t|\\r/','',$result);
        $result = preg_replace('/ {2,}/','',$result);
        return $result;
    }

}
